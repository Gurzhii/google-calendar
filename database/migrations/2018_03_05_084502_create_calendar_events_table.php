<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('calendar_id')->unsigned();
            $table->foreign('calendar_id')->references('id')->on('calendars');

            $table->string('google_calendar_id');
            $table->string('google_calendar_event_id');

            $table->string('background_color');

            $table->string('title')->nullable();
            $table->boolean('all_day_event')->default(false);
            $table->string('status');

            $table->datetime('datetime_start');
            $table->datetime('datetime_end');

            $table->index('calendar_id');
            $table->index('google_calendar_id');
            $table->index('google_calendar_event_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
