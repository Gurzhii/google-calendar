@extends('layout')

@section('content')
    <div class="content">
        @if (!$calendar)
            <h1>Hello!</h1>
            <p class="lead">Please, press "Sync calendars" button</p>
        @else
            {!! $calendar !!}
        @endif
    </div>
@endsection