<?php

namespace App\Extensions;

use App\Queries\CalendarEventsQuery;
use Carbon\Carbon;

class EventsCollection
{
    protected $items;

    public function __construct()
    {
        $this->items = collect(CalendarEventsQuery::getResult());
    }

    public function build()
    {
        $result = [];
        foreach ($this->items as $item) {
            $carbonDateStart = Carbon::parse($item->datetime_start);
            $length = $carbonDateStart->diffInDays(Carbon::parse($item->datetime_end));

            for ($i=0; $i<$length; $i++) {
                if (! isset($result[$carbonDateStart->format('Ymd')]['events'])) {
                    $result[$carbonDateStart->format('Ymd')]['events'] = [];
                }
                $result[$carbonDateStart->format('Ymd')]['year'] = $item->year;
                $result[$carbonDateStart->format('Ymd')]['month'] = $item->month;
                $result[$carbonDateStart->format('Ymd')]['day'] = $item->day;
                $result[$carbonDateStart->format('Ymd')]['events'][$item->id] = (array) $item;
                $result[$carbonDateStart->format('Ymd')]['events'][$item->id]['length'] = $length;

                $carbonDateStart->addDay(1);
            }
        }

        return $result;
    }
}
