<?php

namespace App\Queries;

use Illuminate\Support\Facades\DB;

class CalendarEventsQuery
{
    public static function getResult()
    {
        if (auth()->user()->calendars()->count()) {
            $userCalendarIds = implode(',', auth()->user()->calendars()->get()->pluck('id')->toArray());

            $sql = "SELECT id, title, datetime_start, datetime_end, background_color, DATE_FORMAT(`datetime_start`, '%m-%y') as `date_start`, DATE_FORMAT(`datetime_start`, '%m') as `month`, DATE_FORMAT(`datetime_start`, '%y') as `year`, DATE_FORMAT(`datetime_start`, '%d') as `day`, DATE_FORMAT(`datetime_start`, '%Y%m%d') as `full_date_start_string`, DATE_FORMAT(`datetime_end`, '%Y%m%d') as `full_date_end_string` FROM `calendar_events` where ((datetime_start >= curdate()) and (datetime_start <= curdate() + interval 1 year) and calendar_id IN ({$userCalendarIds}) and all_day_event = 1) order by datetime_start asc";

            return DB::select($sql);
        }
        return null;
    }
}
