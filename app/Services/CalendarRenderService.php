<?php

namespace App\Services;

use App\Extensions\EventsCollection;
use Carbon\Carbon;

class CalendarRenderService
{
    protected $data;
    protected $dayNames = [
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat',
    ];

    public function __construct()
    {
        $this->data = (new EventsCollection())->build();
    }
    
    public function html()
    {
        $calendarData = $this->buildCalendarData();

        $html = '<table class="table">';
        $html .= '<thead>';
        $html .= '<tr>';
        for ($i=0; $i<=5; $i++) {
            if ($i == 0) {
                $html .= "<th>M/Y</th>";
            }
            foreach ($this->dayNames as $dayName) {
                $html .= "<th>{$dayName}</th>";
            }
        }
        $html .= '</tr>';
        $html .= '</thead>';
        foreach ($calendarData as $row) {
            $emptyCellsAfterRow = 43 - count($row['days']) - $row['skipCells']; // 43 means days*5 + 1 cell for month name
            $daysWereSkipped = false;
            $html .= "<tr>";
            foreach ($row['days'] as $day) {
                if (! $daysWereSkipped) {
                    for ($i=0; $i<$row['skipCells']; $i++) {
                        if ($i == 0) {
                            $html .= "<td>{$row['monthShort']}-{$row['yearShort']}</td>";
                        } else {
                            $html .= "<td class='empty'></td>";
                        }
                    }
                    $daysWereSkipped = true;
                }
                $html .= "<td>";
                $html .= "<span class='{$day['adClass']}'>{$day['dayNumber']}</span>";
                if ($day['events']) {
                    foreach ($day['events'] as $event) {
                        $html .= "<span style='background-color: {$event['background_color']}' class='event'>";
                        $html .= $event['title'];
                        $html .= "</span>";
                    }
                }
                $html .= "</td>";
            }
            for ($i=1; $i<=$emptyCellsAfterRow; $i++) {
                $html .= "<td class='empty'></td>";
            }
            $html .= "</tr>";
        }
        $html .= '</table>';

        return $html;
    }

    protected function buildCalendarData()
    {
        $calendar = [];
        $begin = new \DateTime('first day of this month');
        $end = (new \DateTime('+1 year'))->modify('last day of this month');

        for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
            if (!isset($calendar[$i->format('Ym')])) {
                $calendar[$i->format('Ym')] = [];
            }
            $carbonDate = Carbon::parse($i->format('Y-m-d'));
            $key = $i->format('Y') . $i->format('m') . $i->format('d');
            $calendar[$i->format('Ym')]['year'] = $i->format('Y');
            $calendar[$i->format('Ym')]['yearShort'] = $i->format('y');
            $calendar[$i->format('Ym')]['monthShort'] = $i->format('M');
            $calendar[$i->format('Ym')]['month'] = $i->format('m');
            if (! isset($calendar[$i->format('Ym')]['skipCells'])) {
                $calendar[$i->format('Ym')]['skipCells'] = $carbonDate->firstOfMonth()
                        ->dayOfWeek + 1; //adding 1 for Month name COL
            }
            $calendar[$i->format('Ym')]['days'][$i->format('d')]['day'] = $i->format('D');
            $calendar[$i->format('Ym')]['days'][$i->format('d')]['adClass'] = null;
            if ($carbonDate->isSaturday()) {
                $calendar[$i->format('Ym')]['days'][$i->format('d')]['adClass'] = 'saturday';
            } elseif ($carbonDate->isSunday()) {
                $calendar[$i->format('Ym')]['days'][$i->format('d')]['adClass'] = 'sunday';
            }
            $calendar[$i->format('Ym')]['days'][$i->format('d')]['dayNumber'] = $i->format('d');
            $calendar[$i->format('Ym')]['days'][$i->format('d')]['events'] =
                (isset($this->data[$key])) ? $this->data[$key]['events'] : null;
        }

        return $calendar;
    }
}
