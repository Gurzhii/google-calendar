<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    protected $googleOAuthRedirectCode;
    protected $googleOAuthClient;

    protected function __construct($OAuthClient, $OAuthRedirectCode)
    {
        $this->googleOAuthClient  = $OAuthClient;
        $this->googleOAuthRedirectCode = $OAuthRedirectCode;
    }

    public static function create($OAuthClient, string $OAuthRedirectCode) : AuthService
    {
        return new static($OAuthClient, $OAuthRedirectCode);
    }
    
    public function login()
    {
        $this->googleOAuthClient->fetchAccessTokenWithAuthCode($this->googleOAuthRedirectCode);

        $plus = new \Google_Service_Plus($this->googleOAuthClient);
        $google_user = $plus->people->get('me');

        Auth::login($this->user($google_user));
    }

    protected function user($user)
    {
        $token = $this->googleOAuthClient->getAccessToken();

        $email = $user['emails'][0]['value'];
        $first_name = $user['name']['givenName'];
        $last_name = $user['name']['familyName'];

        return User::firstOrCreate([
            'email' => $email
        ], [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'google_token' => json_encode($token)
        ]);
    }
}
