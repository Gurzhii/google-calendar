<?php

namespace App\Services;

use App\Calendar;
use App\CalendarEvent;
use Carbon\Carbon;

class SyncCalendarsService
{
    protected $client;
    protected $calendarService;
    protected $fullSync;

    protected $calendarEventColors;

    protected $calendarsList;

    public function __construct()
    {
        $this->client = (new GoogleAuthService())->getClientWithAccessToken();
        $this->calendarService = new \Google_Service_Calendar($this->client);

        $this->calendarsList = $this->calendarService->calendarList->listCalendarList();

        $this->calendarEventColors = $this->calendarService->colors->get()->getEvent();
    }

    public function sync()
    {
        $this->syncCalendars();
        $this->syncEvents();
    }

    public function syncCalendars()
    {
        $calendars = auth()->user()->calendars()->get();

        if ($calendars->count()) {
            $this->fullSync = false;
        } else {
            $this->fullSync = true;
        }

        foreach ($this->calendarsList->getItems() as $calendar) {
            Calendar::updateOrCreate([
                'user_id' => auth()->id(),
                'google_id' => $calendar->getId(),
            ], [
                'title' => $calendar->getSummary(),
                'background_color' => $calendar->getBackgroundColor(),
                'description' => $calendar->getDescription(),
                'timezone' => $calendar->getTimezone()
            ]);
        }
    }
    
    public function syncEvents()
    {
        $calendars = auth()->user()->calendars()->get();

        foreach ($calendars as $calendar) {
            $calendarWithEvents = $this->calendarService->events->ListEvents(
                $calendar->getGoogleId(),
                $this->getListEventsParams($calendar->getGoogleId())
            );
            $this->updateCalendarSyncToken($calendar->getGoogleId(), $calendarWithEvents->getNextSyncToken());

            foreach ($calendarWithEvents->getItems() as $event) {
                if ($event->getStatus() == 'cancelled') {
                    continue;
                }

                if ($event->getEnd() != null) {
                    $allDayEvent = ($event->getEnd()->getDateTime()) ? false : true;
                } else {
                    $allDayEvent = false;
                }

                if ($allDayEvent) {
                    $dateStart = Carbon::parse($event->getStart()->getDate())->toDateString();
                    $dateEnd = Carbon::parse($event->getEnd()->getDate())->toDateString();
                } else {
                    $dateStart = Carbon::parse($event->getStart()->getDateTime())->toDateTimeString();
                    $dateEnd = Carbon::parse($event->getEnd()->getDateTime())->toDateTimeString();
                }

                CalendarEvent::updateOrCreate([
                    'google_calendar_id' => $calendar->getGoogleId(),
                    'google_calendar_event_id' => $event->getId()
                ], [
                    'calendar_id' => $calendar->id,
                    'title' => $event->getSummary(),
                    'status' => $event->getStatus(),
                    'datetime_start' => $dateStart,
                    'datetime_end' => $dateEnd,
                    'background_color' => ($event->getColorId()) ? $this->calendarEventColors[$event->getColorId()]->background : $calendar->background_color,
                    'all_day_event' => $allDayEvent
                ]);
            }
        }
    }

    protected function updateCalendarSyncToken($googleCalendarId, $nextSyncToken)
    {
        auth()->user()->calendars()->where('google_id', $googleCalendarId)->update([
            'sync_token' => $nextSyncToken
        ]);
    }

    protected function getListEventsParams($googleCalendarId)
    {
        $params = [];

        if ($this->fullSync) {
            $params['maxResults'] = 2500;
            $params['timeMin'] = (new Carbon('first day of this month'))->setTime(0, 0, 0)->toRfc3339String();
            $params['showDeleted'] = false;
            $params['singleEvents'] = false;
        } else {
            $sync_token = auth()->user()
                ->calendars()->where('google_id', $googleCalendarId)->first()
                ->getAttribute('sync_token');
            if ($sync_token) {
                $params['syncToken'] = $sync_token;
            }
        }

        return $params;
    }
}
