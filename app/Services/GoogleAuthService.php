<?php

namespace App\Services;

class GoogleAuthService
{
    protected $client;

    public function __construct()
    {
        $this->client = new \Google_Client();

        $this->client->setClientId(env('GOOGLE_CLIENT_ID'));
        $this->client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $this->client->setRedirectUri(env('GOOGLE_REDIRECT_URL'));
        $this->client->setScopes(explode(',', env('GOOGLE_SCOPES')));
        $this->client->setApprovalPrompt(env('GOOGLE_APPROVAL_PROMPT'));
        $this->client->setAccessType(env('GOOGLE_ACCESS_TYPE'));
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getClientWithAccessToken($token = null)
    {
        if (! $token) {
            $token = auth()->user()->google_token;
        }

        $this->client->setAccessToken($token);

        return $this->client;
    }
}
