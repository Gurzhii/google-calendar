<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $guarded = [];

    public function getGoogleId()
    {
        return $this->google_id;
    }
}
