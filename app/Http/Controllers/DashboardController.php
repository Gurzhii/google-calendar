<?php

namespace App\Http\Controllers;

use App\Services\CalendarRenderService;
use App\Services\SyncCalendarsService;

class DashboardController extends Controller
{
    public function index()
    {
        if (auth()->user()->calendars()->count() == 0) {
            (new SyncCalendarsService())->sync();
        }
        $calendar = (new CalendarRenderService())->html();

        return view('dashboard.index', compact('calendar'));
    }
}
