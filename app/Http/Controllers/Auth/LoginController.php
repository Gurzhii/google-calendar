<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\GoogleAuthService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(GoogleAuthService $googleAuthService, User $user, Request $request)
    {
        $client = $googleAuthService->getClient();

        if ($request->has('code')) {
            AuthService::create($client, request()->get('code'))->login();

            return redirect('/dashboard');
        } else {
            $auth_url = $client->createAuthUrl();

            return redirect($auth_url);
        }
    }
    
    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
