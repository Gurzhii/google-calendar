<?php

namespace App\Http\Controllers;

use App\Services\SyncCalendarsService;

class CalendarsController extends Controller
{
    public function store()
    {
        (new SyncCalendarsService)->sync();

        return redirect()->back();
    }
}
